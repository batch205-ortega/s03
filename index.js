// To create a class in JS, we use the class keyword and {}.
// Naming convention for classes: Begins with UpperCase Characters.
/*
	class Name {
	
	};
*/

class Dog {

	// We add a constructor method to a class to be able to initialize values upon instantiation of an object from a class.
	// "Instantiation" is the technical term when creating an object from a class.
	// An object created from a class is called an instance.
	constructor(name,breed,age){

		this.name = name;
		this.breed = breed;
		this.age = age * 7;

	}

};

let dog1 = new Dog("Bantay","chihuahua",3);

console.log(dog1);

/*
	Create a new class called Person.

	This Person class should be able to instantiate a new object with the following fields:

	name,
	age,
	nationality,
	address

	Instantiate 2 new objects from the Person class as person1 and person2.

	Log both objects in the console. Take a screenshot of your console and send it to the hangouts.

*/

class Person {

	constructor(name,age,nationality,address){

		this.name = name;
		// this.age = age;
		this.nationality = nationality;
		this.address = address;

		if(typeof age!== "number"){
			this.age = undefined;
		} else {
			this.age = age;
		};

	};

	greet() {
		console.log(`Hello! Good Morning!`);
		// this must be return if the intended method can be chained.
		return this;
	};
	introduce() {
		// We should be able to return this, so that when used in a chain, we can still have an access to the reference of this
		console.log(`Hi! My name is ${this.name}`);
		return this;
	};

	changeAddress(newAddress){

		// We can also update the field of our object by referring to the property via this keyword and reassigning a new value.
		this.address = newAddress;
		return this;
	};

};

let person1 = new Person("Monkey D. Luffy",19,"East Blue","Dawn Island");
let person2 = new Person("Roronoa Zoro",21,"East Blue","Kuraigana Island");

console.log(person1);
console.log(person2);

// You can actually also chain methods from an instance
person1.greet().introduce();
person2.greet().introduce();

class Student {

	constructor(name,email,grades){

		// name
		if(typeof name !== "string"){
			this.name = undefined;
		} else {
			this.name = name;
		};

		// email
		if(typeof email !== "string"){
			this.email = undefined;
		} else {
			this.email = email;
		};

		// grades
		if(typeof grades !== "object"){
			this.grades = undefined;
		} else if (grades.every(grade => typeof grade !== "number")) {
			this.grades = undefined;
		} else {
			this.grades = grades;
		};

		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

	};

	login() {
	    console.log(`${this.email} has logged in`);
	};
	logout() {
	    console.log(`${this.email} has logged out`);
	};
	listGrades() {
	    this.grades.forEach(grade => {
	        console.log(grade);
	    });
	};
	computeAve() {
		const ave = (this.grades.reduce((a,b) => a + b))/(this.grades.length);
		this.average = ave;
		return this;
	};
	willPass() {
		this.computeAve();

		if(Math.round(this.average) >= 85){
			this.isPassed = true;
			return this;
		} else {
			this.isPassed = false;
			return this;
		};
	};
	willPassWithHonors() {
		this.computeAve();
		this.willPass();

		if(Math.round(this.average) >= 90){
			this.isPassedWithHonors = true;
			return this;
		} else if (this.isPassed && Math.round(this.average) < 90) {
			this.isPassedWithHonors = false;
			return this;
		} else {
			return this;
		};
	};

};

let student1 = new Student('John','john@mail.com',[89, 84, 78, 88]);
let student2 = new Student('Joe','joe@mail.com',[78, 82, 79, 85]);
let student3 = new Student('Jane','jane@mail.com',[87, 89, 91, 93]);
let student4 = new Student('Jessie','jessie@mail.com',[91, 89, 92, 93]);

console.log(student1);
console.log(student2);
console.log(student3);
console.log(student4);